#include "ModemStatemachine.h"

#include "PoweredOffState.h"
#include "UnknownState.h"
#include "UninitializedState.h"
#include "InitializedState.h"
#include "GnssXtraState.h"

namespace
{
    BG95 modem;
    UnknownState unknownState(modem);
    PoweredOffState poweredOffState(modem);
    UninitializedState uninitializedState(modem);
    InitializedState initializedState(modem);
    GnssXtraState gnssXtraState(modem);

    Transition<EState, EEvent> transitions[] = {
        {EState::UNKNOWN,       EEvent::MODEM_NOT_RESPONDING, &poweredOffState},
        {EState::UNKNOWN,       EEvent::POWER_ON, &uninitializedState},
        {EState::POWERED_OFF,   EEvent::POWER_ON,             &uninitializedState},
        {EState::UNINITIALIZED, EEvent::INITIALIZED_MODEM,    &initializedState},
        {EState::UNINITIALIZED, EEvent::INITIALIZE_XTRA,    &gnssXtraState},
        {EState::GNSS_XTRA, EEvent::INITIALIZED_MODEM,    &initializedState},
        {EState::ALL,           EEvent::RESET_MODEM,          &poweredOffState},
    };
}

ModemStatemachine::ModemStatemachine() :
      Statemachine(transitions, sizeof(transitions)/sizeof(transitions[0]), &unknownState, EEvent::NONE, EState::ALL)
{
}
