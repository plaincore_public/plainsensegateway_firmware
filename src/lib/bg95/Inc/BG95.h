#pragma once

#include <cstdint>
#include <string>

#define UART_RX_SIZE (uint16_t) 256

class BG95
{
public:
    void resetModule();

    void turnOnModule();

    bool transmitAtc(const char _out[], uint32_t maxResponseTimeMs = 300, const std::string& expectedOutput = "OK");

    uint8_t * getLine(uint16_t & lineLength);

    char* getImei();

    void readImei();

    bool isModemConnectedToNetwork();

    int getRssi();

private:
    uint8_t cursorPos = 0;


    /**
     * Uart receive buffer
     */
    uint8_t m_data[UART_RX_SIZE]{};
    char m_imei[16]{}; // IMEI is 15 digits + \0
};