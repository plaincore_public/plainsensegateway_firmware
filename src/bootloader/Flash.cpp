#include "flash.h"

#include "Uart.h"

Flash::Flash(uint32_t appAddress) : appAddress(appAddress)
{
}

void Flash::flashWord(uint32_t dataToFlash)
{
    if (flashStatus == Unlocked)
    {
        volatile HAL_StatusTypeDef status;
        uint8_t flash_attempt = 0;
        uint32_t address;
        do
        {
            address = appAddress + flashedOffset;
            status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address, dataToFlash);
            flash_attempt++;
        } while (status != HAL_OK && flash_attempt < 10 && dataToFlash == readWord(address));
        if (status != HAL_OK)
        {
            Uart::debugPrint("Flashing Error\n");
        }
        else
        {
            // Word Flash Successful
            flashedOffset += 4;
        }
    }
    else
    {
        Uart::debugPrint("Memory not unlocked\n");
    }
}

uint32_t Flash::readWord(uint32_t address)
{
    uint32_t read_data;
    read_data = *(uint32_t *)(address);
    return read_data;
}

void Flash::eraseMemory()
{
    /* Unock the Flash to enable the flash control register access *************/
    while (HAL_FLASH_Unlock() != HAL_OK)
        while (HAL_FLASH_Lock() != HAL_OK)
            ; // Weird fix attempt

    /* Allow Access to option bytes sector */
    while (HAL_FLASH_OB_Unlock() != HAL_OK)
        while (HAL_FLASH_OB_Lock() != HAL_OK)
            ; // Weird fix attempt

    /* Fill EraseInit structure*/
    FLASH_EraseInitTypeDef EraseInitStruct;
    EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
    EraseInitStruct.PageAddress = appAddress;
    EraseInitStruct.NbPages = FLASH_BANK_SIZE / FLASH_PAGE_SIZE_USER;
    uint32_t PageError;

    volatile HAL_StatusTypeDef status_erase;
    status_erase = HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);

    /* Lock the Flash to enable the flash control register access *************/
    while (HAL_FLASH_Lock() != HAL_OK)
        while (HAL_FLASH_Unlock() != HAL_OK)
            ; // Weird fix attempt

    /* Lock Access to option bytes sector */
    while (HAL_FLASH_OB_Lock() != HAL_OK)
        while (HAL_FLASH_OB_Unlock() != HAL_OK)
            ; // Weird fix attempt

    if (status_erase != HAL_OK)
    {
        // error
    }
    flashStatus = Erased;
    flashedOffset = 0;
}

void Flash::unlockFlashAndEraseMemory()
{
    /* Unock the Flash to enable the flash control register access *************/
    while (HAL_FLASH_Unlock() != HAL_OK)
        while (HAL_FLASH_Lock() != HAL_OK)
            ; // Weird fix attempt

    /* Allow Access to option bytes sector */
    while (HAL_FLASH_OB_Unlock() != HAL_OK)
        while (HAL_FLASH_OB_Lock() != HAL_OK)
            ; // Weird fix attempt

    if (flashStatus != Erased)
    {
        /* Fill EraseInit structure*/
        FLASH_EraseInitTypeDef EraseInitStruct;
        EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
        EraseInitStruct.PageAddress = appAddress;
        EraseInitStruct.NbPages = FLASH_BANK_SIZE / FLASH_PAGE_SIZE_USER;
        uint32_t PageError;

        volatile HAL_StatusTypeDef status_erase;
        status_erase = HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);

        if (status_erase != HAL_OK)
        {
            // error
        }
    }

    flashStatus = Unlocked;
}

void Flash::lockFlash()
{
    /* Lock the Flash to enable the flash control register access *************/
    while (HAL_FLASH_Lock() != HAL_OK)
        while (HAL_FLASH_Unlock() != HAL_OK)
            ; // Weird fix attempt

    /* Lock Access to option bytes sector */
    while (HAL_FLASH_OB_Lock() != HAL_OK)
        while (HAL_FLASH_OB_Unlock() != HAL_OK)
            ; // Weird fix attempt

    flashStatus = Locked;
}