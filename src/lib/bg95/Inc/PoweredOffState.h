#pragma once

#include "ModemStatemachine.h"
#include "IModemState.h"

class PoweredOffState : public IModemState<EState, EEvent>
{
public:
    explicit PoweredOffState(BG95 & modem) : IModemState<EState, EEvent>(EState::POWERED_OFF, modem){}

    void entry() override;

    EEvent update() override;

    void exit() override;
private:
    uint8_t numreplied = 0;
    uint8_t tries = 0;
    uint8_t resetTries = 0;
};