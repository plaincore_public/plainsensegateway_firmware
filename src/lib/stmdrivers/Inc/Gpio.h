#pragma once

class Gpio
{
public:
    static bool initGpio();
    static void switchFastShutdown(bool level);
    static void setBlueLed(bool level);
    static void setOrangeLed(bool level);
    static void setPwrKeyOn(bool level);
};