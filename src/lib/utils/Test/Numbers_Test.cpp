#include <Numbers.h>
#include <String.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <iostream>

namespace
{
    class NumbersTest : public ::testing::Test
    {

      protected:
        NumbersTest() = default;

        ~NumbersTest() override = default;

        void SetUp() override
        {
        }

        void TearDown() override
        {
        }
    };

    TEST_F(NumbersTest, TestNumbersNmeaMode2)
    {
        std::string line = "+QGPSLOC: 130821.000,49.19521,8.93145,7.5,187.9,3,0.00,0.0,0.0,221223,04";
        auto latitude = Numbers::extractNumber<std::string>(line, "0", 1);
        auto longitude = Numbers::extractNumber<std::string>(line, "0", 2);

        EXPECT_EQ(latitude, "49.19521");
        EXPECT_EQ(longitude, "8.93145");
    }

    TEST_F(NumbersTest, TestFloatToString)
    {
        auto str = Numbers::floatToString(1.2345f, 2);
        EXPECT_EQ(str, "1.23");

        str = Numbers::floatToString(-1.2345f, 2);
        EXPECT_EQ(str, "-1.23");
    }

    TEST_F(NumbersTest, TestNumbers)
    {
        std::string test = "+QGPSLOC: "
                           "133859.000,4911.6629N,00855.8345E,1.9,224.9,2,213.47,9.5,5.1,211223,04";
        test = String::RemoveChar(test, 'E');
        test = String::RemoveChar(test, 'N');

        auto num = Numbers::extractNumber<float>(test, 0.0, 0);
        ASSERT_NEAR(num, 133859.000, 1e-3);

        num = Numbers::extractNumber<float>(test, 0.0, 1);
        ASSERT_NEAR(num, 4911.6629f, 1e-3);

        num = Numbers::extractNumber<float>(test, 0.0, 2);
        ASSERT_NEAR(num, 855.8345f, 1e-3);

        num = Numbers::extractNumber<float>(test, 0.0, 3);
        ASSERT_NEAR(num, 1.9f, 1e-3);

        num = Numbers::extractNumber<float>(test, 0.0, 4);
        ASSERT_NEAR(num, 224.9f, 1e-3);

        num = Numbers::extractNumber<float>(test, 0.0, 5);
        ASSERT_NEAR(num, 2, 1e-3);

        num = Numbers::extractNumber<float>(test, 42.0, 12);
        ASSERT_NEAR(num, 42, 1e-3);

        num = Numbers::extractNumber<float>(test, 42.0, -1);
        ASSERT_NEAR(num, 42, 1e-3);
    }
} // namespace
