if (CMAKE_BUILD_TYPE MATCHES Debug)
    set(opt_level -O0)
else ()
    set(opt_level -Os)
endif ()

if (CMAKE_TOOLCHAIN_FILE)
    set(DIR ${CMAKE_CURRENT_SOURCE_DIR})
    file(GLOB sources
            ${DIR}/*.cpp
            ${DIR}/*.c
            ${DIR}/*.h
            ${DIR}/../common/*.cpp
            ${DIR}/../common/*.c
            ${DIR}/../common/*.h
    )

    # set some project constants
    set(elf_file ${application_name}.elf)
    set(bin_file ${application_name}.bin)
    set(hex_file ${application_name}.hex)
    set(map_file ${application_name}.map)
    set(lss_file ${application_name}.lss)

    add_definitions(-DSTM32L053xx)

    # add sources to elf file
    add_executable(${elf_file} ${sources})

    target_include_directories(${elf_file} PRIVATE "./")
    target_link_libraries(${elf_file} PUBLIC startup quecstatemachine stmdrivers bg95 utils)

    # set additional for compiler and linker: optimization and generate map file
    set(additional_compiler_flags ${opt_level})
    set(additional_linker_flags -Wl,-Map=${map_file},--cref,--no-warn-mismatch)
    target_compile_options(${elf_file} PRIVATE ${additional_compiler_flags})
    target_link_libraries(${elf_file} PRIVATE ${additional_linker_flags})

    target_link_libraries(${elf_file} PUBLIC "-g -Wl,--gc-sections,--print-memory-usage")

    target_link_libraries(${elf_file} PUBLIC -T${CMAKE_SOURCE_DIR}/src/app/linker.ld)
    set_target_properties(${elf_file} PROPERTIES LINK_DEPENDS ${CMAKE_SOURCE_DIR}/src/app/linker.ld)

    add_custom_target(${elf_file}-size DEPENDS ${elf_file} COMMAND ${ARM_SIZE_EXECUTABLE} -B ${elf_file})
    add_custom_target(${lss_file} DEPENDS ${elf_file} COMMAND ${ARM_OBJDUMP_EXECUTABLE} -S ${elf_file} > ${lss_file})
    add_custom_target(${hex_file} DEPENDS ${elf_file} COMMAND ${ARM_OBJCOPY_EXECUTABLE} -Oihex ${elf_file} ${hex_file})
    add_custom_target(${bin_file} DEPENDS ${elf_file} COMMAND ${ARM_OBJCOPY_EXECUTABLE} -Obinary ${elf_file} ${bin_file})
else()
    set(DIR ${CMAKE_CURRENT_SOURCE_DIR})
    file(GLOB sources
            ${DIR}/*.cpp
    )

    set(sim_target plainsense_app_sim)
    add_executable(${sim_target} ${sources})

    target_include_directories(${sim_target} PRIVATE "./")
    target_link_libraries(${sim_target} PUBLIC quecstatemachine stmdrivers bg95 utils)
endif ()