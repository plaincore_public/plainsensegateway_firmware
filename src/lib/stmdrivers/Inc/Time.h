#pragma once

#include <cstdint>

class Time
{
public:
    static void sleepMS(uint32_t ms);

    static uint32_t getMsSinceStart();
};