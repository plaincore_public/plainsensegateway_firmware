#include "InterruptHandler.h"

#include "stm32l0xx.h"

void InterruptHandler::initInterruptVector(uint32_t *vecTableAddress, uint32_t applicationAddress)
{
    // copy vector table from flash to SRAM.
    for (int i = 0; i < 16; i++)
    {
        vecTableAddress[i] = *(__IO uint32_t *)(applicationAddress + (i << 2));
    }

    // map embedded SRAM to 0x00000000, vector table from SRAM is now used
    SYSCFG->CFGR1 = 3;
}
