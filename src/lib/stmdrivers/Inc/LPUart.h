#pragma once

#include <cstdint>
#include <string>

class LPUart {
public:
    static bool initLPUart();
    static uint16_t getNumReceivedBytes(uint16_t bufferSize);
    static void debugPrint( const char _out[]);
    static void startDmaReceive(uint8_t* data, uint16_t size);
    static void abortDmaReceive();
    static void reply(const std::string &reply);
};