cmake_minimum_required(VERSION 3.16)
project(UtilsTest)

set(CMAKE_CXX_STANDARD 17)

include(FetchContent)
FetchContent_Declare(
    googletest
    GIT_REPOSITORY https://github.com/google/googletest.git
    GIT_TAG        release-1.12.0
)

# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

enable_testing()

add_executable(
    Numbers_Test
    Numbers_Test.cpp
)

add_subdirectory(.. utils)

target_include_directories(utils PUBLIC ../Inc/)
target_include_directories(utils PUBLIC .)

target_link_libraries(
    Numbers_Test
    gtest_main
    gmock
    gmock_main
    utils
)

include(GoogleTest)
