cmake_minimum_required(VERSION 3.5)

set(CMAKE_CXX_STANDARD 17)

# set the build type
if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif (NOT CMAKE_BUILD_TYPE)

if (CMAKE_BUILD_TYPE MATCHES Debug)
    message("Debug build.")
elseif (CMAKE_BUILD_TYPE MATCHES Release)
    message("Release build.")
endif ()

# Project specific settings
set(application_name "plainsense_app")
project(${application_name} C CXX ASM)

if (CMAKE_TOOLCHAIN_FILE)
    # add thirdparty libraries
    add_subdirectory(lib)
endif ()

# add main project
add_subdirectory(src)