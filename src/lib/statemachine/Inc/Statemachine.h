#ifndef STM32L0BREAKOUT_STATEMACHINE_H
#define STM32L0BREAKOUT_STATEMACHINE_H

#include "IState.h"
#include "Transition.h"
#include <cstddef>

#include <cstdint>

template <class State, class Event> class Statemachine
{

  public:
    Statemachine(const Transition<State, Event> *transitions, size_t numTransitions, IState<State, Event> *initialState,
                 Event noEvent, State allState)
        : m_transitions(transitions), m_numTransitions(numTransitions), m_currentState(initialState),
          m_noEvent(noEvent), m_allState(allState)
    {
    }

    IState<State, Event> *getCurrentState()
    {
        return m_currentState;
    }

    void doUpdate()
    {
        int numTransitioned = 0;
        Event event;
        do
        {
            if (!m_currentState->isInitialized())
            {
                m_currentState->entry();
                m_currentState->setInitialized();
            }

            event = m_currentState->update();
            if (event != m_noEvent)
            {
                for (size_t i = 0; i < m_numTransitions; i++)
                {
                    Transition<State, Event> transition = m_transitions[i];
                    if ((transition.currentState == m_allState && event == transition.event) ||
                        (m_currentState->getIdentifier() == transition.currentState && event == transition.event))
                    {
                        // change state!
                        m_currentState->exit();
                        m_currentState = transition.nextState;
                        m_currentState->reset();
                        numTransitioned++;
                    }
                }
            }
            if (numTransitioned > m_maxTransitions)
            {
                // go to error state / log error
                break;
            }
        } while (event != m_noEvent);
    }

    void setMaxTransitions(uint8_t maxTransitions)
    {
        m_maxTransitions = maxTransitions;
    }

  private:
    const Transition<State, Event> *m_transitions;
    const size_t m_numTransitions;
    IState<State, Event> *m_currentState;
    Event m_noEvent;
    State m_allState;
    uint8_t m_maxTransitions = 5;
};

#endif // STM32L0BREAKOUT_STATEMACHINE_H
