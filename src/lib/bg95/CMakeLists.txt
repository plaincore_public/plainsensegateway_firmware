set(DIR ${CMAKE_CURRENT_SOURCE_DIR})
file(GLOB LIB_SOURCES
        ${DIR}/Src/*.cpp
        ${DIR}/Inc/*.h
)

add_library(bg95 STATIC ${LIB_SOURCES})

target_link_libraries(bg95 stmdrivers quecstatemachine)
target_include_directories(bg95 PUBLIC ./Inc/)