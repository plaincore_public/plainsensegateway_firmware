#include "LPUart.h"

#include <arpa/inet.h> // Include for inet_pton
#include <cstring>
#include <iostream>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h> // Include for close
#include <vector>

namespace
{
    uint16_t receiveSize = 0;
    uint8_t *receiveBuffer = nullptr;
    uint8_t *receiveBufferPos = nullptr;
    uint16_t bytesReceived = 0;

    bool inTransparentMode = false;
    std::vector<char> transparentBuffer;

    std::string ok("OK\r\n");
} // namespace

bool sendUdpMessage(const std::string &hostIP, uint16_t hostPort, const uint8_t *data, size_t dataSize)
{
    // Create UDP socket
    int udpSocket = socket(AF_INET, SOCK_DGRAM, 0);

    if (udpSocket == -1)
    {
        std::cerr << "Error creating UDP socket\n";
        return false;
    }

    // Define the server address
    sockaddr_in serverAddress{};
    std::memset(&serverAddress, 0, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(hostPort);

    // Convert host IP from string to binary
    if (inet_pton(AF_INET, hostIP.c_str(), &(serverAddress.sin_addr)) <= 0)
    {
        std::cerr << "Error converting host IP\n";
        close(udpSocket);
        return false;
    }

    // Send the message
    ssize_t bytesSent = sendto(udpSocket, data, dataSize, 0, reinterpret_cast<struct sockaddr *>(&serverAddress),
                               sizeof(serverAddress));

    if (bytesSent == -1)
    {
        std::cerr << "Error sending data via UDP\n";
        close(udpSocket);
        return false;
    }

    std::cout << "Sent " << bytesSent << " bytes via UDP\n";

    // Close the UDP socket
    close(udpSocket);

    return true;
}

bool LPUart::initLPUart()
{
    return true;
}

uint16_t LPUart::getNumReceivedBytes(uint16_t bufferSize)
{
    return bytesReceived;
}

void LPUart::startDmaReceive(uint8_t *data, uint16_t size)
{
    receiveBuffer = data;
    receiveBufferPos = data;
    receiveSize = size;
    bytesReceived = 0;
}

void LPUart::abortDmaReceive()
{
}

void LPUart::debugPrint(const char _out[])
{
    std::string str(_out);

    if (inTransparentMode)
    {
        if (str == "+++")
        {
            inTransparentMode = false;

            std::string message(transparentBuffer.begin(), transparentBuffer.end());
            std::cout << "Sending message via UDP: \n" << message << std::endl;

            if (!sendUdpMessage("94.16.106.227", 7678, reinterpret_cast<const uint8_t *>(transparentBuffer.data()),
                                transparentBuffer.size()))
            {
                std::cerr << "Failed to send UDP message\n";
            }

            transparentBuffer.clear();
            reply("\r\n");
            reply(ok);
        }
        else
        {
            for (char i : str)
            {
                transparentBuffer.emplace_back(i);
            }
        }
        return;
    }

    if (str == "AT\r\n" || str == "ATE0\r\n")
    {
        reply("\r\n");
        reply(ok);
    }
    else if (str == "AT+QGPSXTRA?\r\n")
    {
        reply("\r\n");
        reply("+QGPSXTRA: 1\r\n");
        reply(ok);
    }
    else if (str == "AT+CPIN?\r\n")
    {
        reply("\r\n");
        reply("+CPIN: READY\r\n");
        reply(ok);
    }
    else if (str == "AT+CEREG?\r\n")
    {
        reply("\r\n");
        reply("+CEREG: 0,5\r\n");
        reply(ok);
    }
    else if (str == "AT+CGREG?\r\n")
    {
        reply("\r\n");
        reply("+CGREG: 0,5\r\n");
        reply(ok);
    }
    else if (str == "AT+QGMR\r\n")
    {
        reply("\r\n");
        reply("BG95M3-xxxxxxx\r\n");
        reply(ok);
    }
    else if (str == "AT+CFUN=0\r\n")
    {
        reply("\r\n");
        reply(ok);
    }
    else if (str == "AT+CFUN=1\r\n")
    {
        reply("\r\n");
        reply(ok);
    }
    else if (str == "AT+QCFG=\"nwscanseq\",020103\r\n")
    {
        reply("\r\n");
        reply(ok);
    }
    else if(str == "AT+CGDCONT=1,\"IP\",\"iot.1nce.net\"\r\n")
    {
        reply("\r\n");
        reply(ok);
    }
    else if (str == "AT+QGPS=1,1\r\n")
    {
        reply("\r\n");
        reply(ok);
    }
    else if (str == "AT+QGPSLOC=2\r\n")
    {
        reply("\r\n");
        reply("+QGPSLOC: 130821.000,46.29521,7.63145,7.5,187.9,3,0.00,0.0,0.0,221223,04\r\n");
        reply(ok);
    }
    else if (str == "AT+QGPSEND\r\n")
    {
        reply("\r\n");
        reply(ok);
    }
    else if (str == "AT+CSQ\r\n")
    {
        reply("\r\n");
        reply("+CSQ: 20,99\r\n");
        reply(ok);
    }
    else if (str == "AT+QIACT=1\r\n")
    {
        reply("\r\n");
        reply(ok);
    }
    else if (str == "AT+CBC\r\n")
    {
        reply("\r\n");
        reply("+CBC: 0,80,4200\r\n");
        reply(ok);
    }
    else if (str == "AT+QIOPEN=1,0,\"UDP\",\"94.16.106.227\",7678,0,2\r\n")
    {
        reply("\r\n");
        reply("CONNECT\r\n");
        reply(ok);
        inTransparentMode = true;
    }
    else if (str == "AT+CGSN\r\n")
    {
        reply("\r\n");
        reply("567343567683245\r\n");
        reply(ok);
    }
    else if (str == "AT+QICLOSE=0\r\n")
    {
        reply("\r\n");
        reply(ok);
    }
    else
    {
        std::cerr << "Unknown AT command: " << str << std::endl;
        reply(ok);
    }
}

void LPUart::reply(const std::string &reply)
{
    for (char i : reply)
    {
        *receiveBufferPos++ = i;
    }
    bytesReceived += reply.size();
}
