#include "GnssXtraState.h"


void GnssXtraState::entry()
{
}

EEvent GnssXtraState::update()
{
    // initialize GNSS Xtra
    uint16_t length = 0;
    m_modem->transmitAtc("AT+QGPSXTRA?\r\n");
    uint8_t *str = m_modem->getLine(length);
    if (std::string(reinterpret_cast<char *>(str), length) != "+QGPSXTRA: 1\r\n")
    {
        // if newly set, will only take effect after reboot
        m_modem->transmitAtc("AT+QGPSXTRA=1\r\n");
        m_modem->resetModule();
        return EEvent::RESET_MODEM;
    }

    m_modem->transmitAtc("AT+QGPSXTRATIME?\r\n");
    m_modem->transmitAtc("AT+QGPSXTRADATA?\r\n");
    m_modem->transmitAtc("AT+QGPSCFG=\"xtra_info\"\r\n");

    // maybe not needed since download will be triggered when starting GNSS
    m_modem->transmitAtc("AT+QGPSCFG=\"xtra_download\",1\r\n");

    return EEvent::INITIALIZED_MODEM;
}

void GnssXtraState::exit()
{

}
