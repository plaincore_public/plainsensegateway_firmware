#include "PoweredOffState.h"

#include "Gpio.h"
#include "Uart.h"

void PoweredOffState::entry()
{
    tries = 0;
    resetTries = 0;
    numreplied = 0;
    Uart::debugPrint("Turn on Modem\r\n");
    m_modem->turnOnModule();
}

EEvent PoweredOffState::update()
{
    // wait until modem responds
    if (m_modem->transmitAtc("AT\r\n"))
    {
        // TODO: Not a nice way to handle this. Rather wait and move back to UnknownState
        if (numreplied == 9)
        {
            // module is already running and responding and not in a boot loop
            return EEvent::POWER_ON;
        }
        numreplied++;
        return EEvent::NONE;
    }

    numreplied = 0;
    tries++;

    if (tries > 20)
    {
        if (resetTries == 0)
        {
            // TODO: strange hack
            Uart::debugPrint("Reset module\r\n");
            m_modem->resetModule();
            resetTries = 1;
            return EEvent::NONE;
        }

        // Modem did not respond, reset it
        // TODO: do proper reset state here.
        return EEvent::RESET_MODEM;
    }

    return EEvent::NONE;
}

void PoweredOffState::exit()
{
}
