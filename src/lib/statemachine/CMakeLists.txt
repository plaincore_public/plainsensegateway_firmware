add_library(quecstatemachine STATIC
    ./Src/Statemachine.cpp
)

target_include_directories(quecstatemachine PUBLIC ./Inc/)
target_link_libraries(quecstatemachine PUBLIC utils)

if (!CMAKE_TOOLCHAIN_FILE)
    add_subdirectory(Test)
endif ()
