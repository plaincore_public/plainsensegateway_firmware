set(DIR ${CMAKE_CURRENT_SOURCE_DIR})

if (CMAKE_TOOLCHAIN_FILE)
    file(GLOB LIB_SOURCES
            ${DIR}/Src/*.cpp
            ${DIR}/Inc/*.h
    )
else ()
    # Simulated stm drivers
    file(GLOB LIB_SOURCES
            ${DIR}/SrcSim/*.cpp
            ${DIR}/Inc/*.h
    )
endif ()

add_library(stmdrivers STATIC ${LIB_SOURCES})

if (CMAKE_TOOLCHAIN_FILE)
    target_compile_definitions(stmdrivers PRIVATE -DSTM32L053xx)
    target_link_libraries(stmdrivers PUBLIC stm32l0xx)
endif ()

target_include_directories(stmdrivers PUBLIC ./Inc/)