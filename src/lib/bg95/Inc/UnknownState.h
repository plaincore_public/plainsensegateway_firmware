#pragma once

#include "ModemStatemachine.h"
#include "IModemState.h"

class UnknownState : public IModemState<EState, EEvent>
{
public:
    explicit UnknownState(BG95 & modem) : IModemState<EState, EEvent>(EState::UNKNOWN, modem){}

    void entry() override;

    EEvent update() override;

    void exit() override;

};
