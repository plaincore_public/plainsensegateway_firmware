#pragma once

#include <cstdint>

class InterruptHandler{
public:
    static void initInterruptVector(uint32_t* vecTableAddress, uint32_t applicationAddress);
};