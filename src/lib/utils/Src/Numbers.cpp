#include "Numbers.h"
#include <cmath>

std::string Numbers::floatToString(float value, int precision)
{
    bool isNegative = (value < 0);
    if (isNegative)
    {
        value = -value;
    }

    int intValue = static_cast<int>(std::round(value * 100));

    std::string result = std::to_string(intValue);

    result.insert(result.length() - precision, ".");

    if (isNegative)
    {
        result = '-' + result;
    }
    return result;
}
