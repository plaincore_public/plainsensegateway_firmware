#ifndef __FLASH_H__
#define __FLASH_H__

#include "stm32l0xx.h"
#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal_flash.h"

#include "string"
#include "cstring"

//#define FLASH_BANK_SIZE (0XA800)		//22kB
//#define FLASH_PAGE_SIZE_USER (0x400)	//1kB

#define FLASH_BANK_SIZE (0xB000)		//44kB
#define FLASH_PAGE_SIZE_USER (0x80)	    //128 bytes

typedef enum
{
    Erased,
    Unlocked,
    Locked
} FlashStatus;

class Flash
{
public:
    Flash(uint32_t appAddress);

    void flashWord(uint32_t word);

    void eraseMemory();

    void unlockFlashAndEraseMemory();

    void lockFlash();

private:
    static uint32_t readWord(uint32_t address);
    void debugPrint(const char _out[]);

    FlashStatus flashStatus = Locked;
    long flashedOffset = 0;
    uint32_t appAddress;
};

#endif /* __FLASH_H__ */