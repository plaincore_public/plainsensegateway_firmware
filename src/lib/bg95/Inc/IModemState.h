#pragma once

#include "IState.h"
#include "BG95.h"

template<class State, class Event>
class IModemState : public IState<State, Event>
{
public:
    IModemState(EState identifier, BG95 & modem) : IState<EState, EEvent>(identifier), m_modem(&modem)
    {}

protected:
    BG95 *m_modem;
};