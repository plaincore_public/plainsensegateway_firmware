#include "Uart.h"

#include <iostream>

bool Uart::initUart()
{
    return true;
}

void Uart::debugPrint(const char _out[])
{
    std::cout << _out << std::flush;
}

void Uart::transmitBlocking(uint8_t *pData, uint16_t size, uint32_t timeout)
{
    std::string str((char *)pData);
    std::cout << str.substr(0, size) << std::flush;
}

uint16_t Uart::getNumReceivedBytes(uint16_t buffSize)
{
    return 0;
}

void Uart::receiveDma(uint8_t *pData, uint16_t size)
{
}

void Uart::abortReceive()
{
}
