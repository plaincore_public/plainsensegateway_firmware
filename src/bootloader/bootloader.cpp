#include "cstdlib"
#include "cstring"
#include "stm32l0xx.h"
#include "stm32l0xx_hal.h"

#include "Clock.h"
#include "Dma.h"
#include "Gpio.h"
#include "HALUtils.h"
#include "Uart.h"

#include "flash.h"

#define APPLICATION_ADDRESS (uint32_t)0x08005000
#define UART_RX_SIZE (uint16_t)600

/**
 * Uart receive buffer
 */
uint8_t data[UART_RX_SIZE];

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler()
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while (true)
    {
    }
    /* USER CODE END Error_Handler_Debug */
}

typedef void(application_t)();
typedef struct
{
    uint32_t stack_addr;   // Stack Pointer
    application_t *func_p; // Program Counter
} JumpStruct;

void jumpToApp(const uint32_t address)
{
    const JumpStruct *vector_p = (JumpStruct *)address;

    /* let's do The Jump! */
    /* Jump, used asm to avoid stack optimization */
    asm("msr msp, %0; bx %1;" : : "r"(vector_p->stack_addr), "r"(vector_p->func_p));
}

uint16_t getNumReceivedBytes()
{
    return Uart::getNumReceivedBytes(UART_RX_SIZE);
}

void handleAppTransmission()
{
    Flash flash(APPLICATION_ADDRESS);
    flash.unlockFlashAndEraseMemory();

    Uart::receiveDma(data, UART_RX_SIZE);
    Uart::debugPrint("data_size?\r\n");
    while (getNumReceivedBytes() < 4)
    {
    }
    Uart::abortReceive();

    // we have received four bytes --> data size information
    uint32_t app_size = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);

    while (app_size > 0)
    {
        Gpio::setOrangeLed(false);
        Uart::receiveDma(data, UART_RX_SIZE);
        Uart::debugPrint("+\r\n");

        uint16_t bytesToReceive = app_size < 512 ? app_size : 512;
        uint16_t test = 0;
        while (test < bytesToReceive)
        {
            test = getNumReceivedBytes();
        }

        Uart::abortReceive();
        app_size -= bytesToReceive;

        Gpio::setOrangeLed(true);
        for (uint16_t i = 0; i < bytesToReceive; i = i + 4)
        {
            uint32_t word = data[i] | (data[i + 1] << 8) | (data[i + 2] << 16) | (data[i + 3] << 24);
            flash.flashWord(word);
        }
    }
    flash.lockFlash();
}

extern "C" int main()
{
    HALUtils::HALInit();
    if (!Clock::initMainApp())
    {
        Error_Handler();
    }
    if (!Gpio::initGpio())
    {
        Error_Handler();
    }
    if (!Dma::initDma())
    {
        Error_Handler();
    }
    if (!Uart::initUart())
    {
        Error_Handler();
    }

    Uart::receiveDma(data, UART_RX_SIZE);
    Uart::debugPrint("Waiting for app update...\r\n");
    for (int i = 0; i < 3; i++)
    {
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_SET);
        HAL_Delay(300);
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);
        HAL_Delay(300);

        char buf;
        Uart::debugPrint(itoa(3 - i, &buf, 10));
        Uart::debugPrint("\r\n");
    }
    Uart::abortReceive();
    Gpio::setBlueLed(false);

    if (strncmp(reinterpret_cast<const char *>(data), "Update please", 13) != 0)
    {
        Uart::debugPrint("No app transmission detected\r\n");
    }
    else
    {
        Uart::debugPrint("Updating application\r\n");
        handleAppTransmission();
    }
    Uart::debugPrint("Booting to main app...\r\n");

    __HAL_RCC_USART1_CLK_DISABLE();
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9 | GPIO_PIN_10);

    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_6);
    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_7);
    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_8);
    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_9);
    __HAL_RCC_GPIOC_CLK_DISABLE();
    HAL_RCC_DeInit();
    HAL_DeInit();
    SysTick->CTRL = 0;
    SysTick->LOAD = 0;
    SysTick->VAL = 0;

    jumpToApp(APPLICATION_ADDRESS);
}
