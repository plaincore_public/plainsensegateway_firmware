#pragma once

#include <string>

namespace String
{
    std::string RemoveChar(const std::string& str, char c);
}