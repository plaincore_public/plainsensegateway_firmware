#pragma once

#include <cstdint>

class Uart
{

public:
    static bool initUart();
    static void debugPrint(const char _out[]);
    static void transmitBlocking(uint8_t *pData, uint16_t size, uint32_t timeout);
    static uint16_t getNumReceivedBytes(uint16_t buffSize);
    static void receiveDma(uint8_t *pData, uint16_t size);
    static void abortReceive();
};