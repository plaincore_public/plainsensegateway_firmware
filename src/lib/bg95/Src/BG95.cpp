#include "BG95.h"

#include "Gpio.h"
#include "LPUart.h"
#include "Numbers.h"
#include "Time.h"
#include "Uart.h"

#include <cassert>
#include <string>

void BG95::resetModule()
{
    // pwrkey trigger reset impulse
    Gpio::switchFastShutdown(true);
    Time::sleepMS(200);

    // falling edge triggers fast shutdown
    Gpio::switchFastShutdown(false);
    Time::sleepMS(1000);
    Gpio::switchFastShutdown(true);

    Time::sleepMS(3000);
}

void BG95::turnOnModule()
{
    cursorPos = 0;
    // VBAT should be high at least 30ms before triggering power key
    Time::sleepMS(35);

    // pwrkey trigger on impulse
    Gpio::setPwrKeyOn(true);
    Time::sleepMS(150);
    Gpio::setPwrKeyOn(false);
    Time::sleepMS(700);
    Gpio::setPwrKeyOn(true);

    // wait for Modem UART to be active.
    Time::sleepMS(2550);
}

void BG95::readImei()
{
    transmitAtc("AT+CGSN\r\n");
    uint16_t length = 0;
    uint8_t *line = getLine(length);
    for (int i = 0; i < length; i++)
    {
        m_imei[i] = line[i];
    }
    m_imei[15] = '\0';
}

bool BG95::transmitAtc(const char _out[], uint32_t maxResponseTimeMs, const std::string &expectedOutput)
{
    Uart::debugPrint("->: ");
    Uart::debugPrint(_out);

    cursorPos = 2; // skip initial newline
    LPUart::startDmaReceive(m_data, UART_RX_SIZE);
    LPUart::debugPrint(_out);

    uint32_t start = Time::getMsSinceStart();
    bool ok = false;
    while (Time::getMsSinceStart() - start < maxResponseTimeMs)
    {
        std::string message(reinterpret_cast<char *>(m_data), LPUart::getNumReceivedBytes(UART_RX_SIZE));
        if (message.find(expectedOutput) != std::string::npos)
        {
            ok = true;
            break;
        }
        else if (message.find("ERROR") != std::string::npos)
        {
            break;
        }
        Time::sleepMS(15);
    }

    // should abort earlier if "OK" or "ERROR" detected, dont do this busy wait
    LPUart::abortDmaReceive();

    // mirror to debug uart
    Uart::debugPrint("<-: ");
    Uart::transmitBlocking(m_data, LPUart::getNumReceivedBytes(UART_RX_SIZE), 10);
    return ok;
}

char *BG95::getImei()
{
    return m_imei;
}

uint8_t *BG95::getLine(uint16_t &lineLength)
{
    uint16_t received = LPUart::getNumReceivedBytes(UART_RX_SIZE);
    for (uint16_t i = cursorPos; i < received; i++)
    {
        if (m_data[i] == '\r' && m_data[i + 1] == '\n')
        {
            // end of line found
            uint16_t oldCursor = cursorPos;
            cursorPos = i + 2;
            lineLength = cursorPos - oldCursor;
            return &m_data[oldCursor];
        }
    }
    return m_data;
}

int BG95::getRssi()
{
    if (!transmitAtc("AT+CSQ\r\n"))
    {
        return 99;
    }

    uint16_t length = 0;
    uint8_t *str = getLine(length);
    std::string line(reinterpret_cast<char *>(str), length);
    return Numbers::extractNumber<int>(line, 99, 0);
}

bool BG95::isModemConnectedToNetwork()
{
    bool connected = true;
    uint16_t length = 0;

    transmitAtc("AT+CPIN?\r\n");
    uint8_t *str = getLine(length);

    if (std::string(reinterpret_cast<char *>(str), length) != "+CPIN: READY\r\n")
    {
        connected = false;
    }

    transmitAtc("AT+CEREG?\r\n");
    str = getLine(length);
    bool ceregOk = false;
    // 1nce sim card is always roaming. So it must always connect as roaming
    if (std::string(reinterpret_cast<char *>(str), length) == "+CEREG: 0,5\r\n")
    {
        ceregOk = true;
    }

    transmitAtc("AT+CGREG?\r\n");
    str = getLine(length);
    bool cgregOk = false;
    // 1nce sim card is always roaming. So it must always connect as roaming
    if (std::string(reinterpret_cast<char *>(str), length) == "+CGREG: 0,5\r\n")
    {
        cgregOk = true;
    }

    if (!ceregOk && !cgregOk)
    {
        // we do not have GSM nor LTE connection
        connected = false;
    }

    if (getRssi() == 99)
    {
        connected = false;
    }

    return connected;
}
