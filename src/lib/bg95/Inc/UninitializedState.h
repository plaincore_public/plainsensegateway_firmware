#pragma once

#include "ModemStatemachine.h"
#include "IModemState.h"

class UninitializedState : public IModemState<EState, EEvent>
{
public:
    explicit UninitializedState(BG95 & modem) : IModemState<EState, EEvent>(EState::UNINITIALIZED, modem){}

    void entry() override;

    EEvent update() override;

    void exit() override;
};