#include "Dma.h"

#include "../../../../lib/STM32L0xx_HAL_Driver/conf/stm32l0xx_hal_conf.h"

bool Dma::initDma()
{
    /* DMA controller clock enable */
    __HAL_RCC_DMA1_CLK_ENABLE();

    HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0, 0);
    //HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

    HAL_NVIC_SetPriority(DMA1_Channel4_5_6_7_IRQn, 0, 0);
    //HAL_NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);

    return true;
}