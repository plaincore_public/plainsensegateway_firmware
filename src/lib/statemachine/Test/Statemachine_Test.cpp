#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "Statemachine.h"
#include <iostream>


enum EState
{
    ALL,
    POWERED_OFF,
    UNINITIALIZED,
};

enum EEvent
{
    NONE,
    POWER_ON,
    RESET
};

class TestStatemachine : public Statemachine<EState, EEvent>
{
public:
    TestStatemachine(const Transition<EState, EEvent> *transitions,
                     size_t numTransitions,
                     IState<EState, EEvent> *initialState, EEvent noEvent, EState allState);

private:
};

class PowerOffState : public IState<EState, EEvent>
{
private:
    int count = 0;
public:
    PowerOffState() : IState<EState, EEvent>(EState::POWERED_OFF)
    {

    }

    MOCK_METHOD0(entry, void());
    MOCK_METHOD0(exit, void());

    EEvent update() override
    {
        if (count == 0)
        {
            count++;
            return EEvent::POWER_ON;

        }
        return EEvent::NONE;
    }
};


class UninitializedState : public IState<EState, EEvent>
{
private:
    int count = 0;

public:
    UninitializedState() : IState<EState, EEvent>(EState::UNINITIALIZED)
    {

    }

    MOCK_METHOD0(entry, void());
    MOCK_METHOD0(exit, void());

    EEvent update() override
    {
        if (count == 1)
        {
            count = 0;
            return EEvent::RESET;
        }
        count++;
        return EEvent::NONE;
    }
};

TestStatemachine::TestStatemachine(const Transition<EState, EEvent> *transitions,
                                   size_t numTransitions,
                                   IState<EState, EEvent> *initialState, EEvent noEvent, EState allState) :
    Statemachine(transitions,
                 numTransitions,
                 initialState,
                 noEvent, allState)
{
}

namespace
{
    class StatemachineTest : public ::testing::Test
    {

    protected:
        StatemachineTest() = default;

        ~StatemachineTest() override = default;

        void SetUp() override
        {
        }

        void TearDown() override
        {
        }

    };

    TEST_F(StatemachineTest, TestStatemachine)
    {
        PowerOffState poweredOffState;
        UninitializedState uninitializedState;

        EXPECT_CALL(poweredOffState, entry()).Times(2);
        EXPECT_CALL(poweredOffState, exit()).Times(1);
        EXPECT_CALL(uninitializedState, entry()).Times(1);
        EXPECT_CALL(uninitializedState, exit()).Times(1);

        Transition<EState, EEvent> transitions[] = {
            {EState::POWERED_OFF, EEvent::POWER_ON, &uninitializedState},
            {EState::ALL,         EEvent::RESET,    &poweredOffState},
        };
        TestStatemachine testStatemachine{transitions, 2, &poweredOffState, EEvent::NONE, EState::ALL};
        EXPECT_EQ(testStatemachine.getCurrentState()->getIdentifier(), EState::POWERED_OFF);

        testStatemachine.doUpdate();
        EXPECT_EQ(testStatemachine.getCurrentState()->getIdentifier(), EState::UNINITIALIZED);

        testStatemachine.doUpdate();
        EXPECT_EQ(testStatemachine.getCurrentState()->getIdentifier(), EState::POWERED_OFF);
    }
}
