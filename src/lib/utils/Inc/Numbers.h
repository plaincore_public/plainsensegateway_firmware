#pragma once

#include <string>

namespace Numbers
{
    template<typename T>
    T customAtof(const std::string &str, T errorValue)
    {
        T result = 0;
        T factor = 1;
        size_t i = 0;

        for (; i < str.length() && str[i] != '.'; ++i)
        {
            if (str[i] >= '0' && str[i] <= '9')
            {
                result = result * 10 + (str[i] - '0');
            } else
            {
                return errorValue; // Return error value
            }
        }

        if (i < str.length() && str[i] == '.')
        {
            ++i;
            for (; i < str.length(); ++i)
            {
                if (str[i] >= '0' && str[i] <= '9')
                {
                    factor *= 0.1;
                    result += factor * (str[i] - '0');
                } else
                {
                    return errorValue; // Return error value
                }
            }
        }
        return static_cast<T>(result);
    }

    template<typename T>
    T extractNumber(const std::string &line, T errorValue, size_t location)
    {
        size_t colonPosition = line.find(':');
        if (colonPosition == std::string::npos)
        {
            return errorValue; // Return error value
        }

        size_t startPos = colonPosition + 2;
        for (size_t i = 0; i < location; ++i)
        {
            startPos = line.find(',', startPos);
            if (startPos == std::string::npos)
            {
                return errorValue; // Return error value
            }
            ++startPos;
        }

        size_t endPos = line.find(',', startPos);
        if (endPos == std::string::npos)
        {
            endPos = line.size();
        }

        std::string numberString = line.substr(startPos, endPos - startPos);
        if (numberString.empty())
        {
            return errorValue; // Return error value
        }
        if constexpr (std::is_arithmetic_v<T>)
        {
            return customAtof<T>(numberString, errorValue);
        } else
        {
            return numberString;
        }
    }

    std::string floatToString(float value, int precision = 2);
}