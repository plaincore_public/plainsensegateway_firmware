#ifndef STM32L0BREAKOUT_ISTATE_H
#define STM32L0BREAKOUT_ISTATE_H


template<class State, class Event>
class IState
{

public:
    explicit IState(State identifier)
    {
        m_identifier = identifier;
    }

    virtual void entry() = 0;

    virtual Event update() = 0;

    virtual void exit() = 0;

    State getIdentifier()
    {
        return m_identifier;
    }

    bool isInitialized()
    {
        return m_initialized;
    }

    void setInitialized()
    {
        m_initialized = true;
    }

    void reset()
    {
        m_initialized = false;
    }

protected:
    bool m_initialized = false;
    State m_identifier;
};

#endif //STM32L0BREAKOUT_ISTATE_H
