#include "main.h"

#include "Clock.h"
#include "Dma.h"
#include "Gpio.h"
#include "HALUtils.h"
#include "I2C.h"
#include "InterruptHandler.h"
#include "LPUart.h"
#include "ModemStatemachine.h"
#include "Time.h"
#include "Uart.h"

#define APPLICATION_ADDRESS (uint32_t)0x08005000

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler()
{
    // TODO: Move to some abstracted driver __disable_irq();
    while (true)
    {
    }
}

uint32_t __attribute__((section(".vectorTable"))) vect_table_ram[48];

extern "C" int main()
{
    InterruptHandler::initInterruptVector(vect_table_ram, APPLICATION_ADDRESS);

    HALUtils::HALInit();
    if (!Clock::initMainApp())
    {
        Error_Handler();
    }
    if (!Gpio::initGpio())
    {
        Error_Handler();
    }
    if (!Dma::initDma())
    {
        Error_Handler();
    }
    if (!LPUart::initLPUart())
    {
        Error_Handler();
    }
    if (!Uart::initUart())
    {
        Error_Handler();
    }
    if (!I2C::initI2C())
    {
        Error_Handler();
    }

    I2C::initTempSensor();

    ModemStatemachine modemStatemachine;

    while (true)
    {
        modemStatemachine.doUpdate();
        Time::sleepMS(200);
    }
}