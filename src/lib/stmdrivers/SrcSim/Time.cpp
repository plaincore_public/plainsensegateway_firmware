#include "Time.h"
#include <chrono>
#include <unistd.h>

uint64_t timeSinceEpochMillisec()
{
    using namespace std::chrono;
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

void Time::sleepMS(uint32_t ms)
{
    usleep(ms * 1000);
}

uint32_t Time::getMsSinceStart()
{
    return timeSinceEpochMillisec();
}