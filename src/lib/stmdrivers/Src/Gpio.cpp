#include "Gpio.h"
#include "stm32l0xx.h"
#include "stm32l0xx_hal.h"

#include "stm32l0xx_hal_gpio.h"

bool Gpio::initGpio()
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9, GPIO_PIN_RESET);

    GPIO_InitStruct.Pin = GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    // fast shutdown pin
    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitTypeDef GPIO_BUTTON_InitStruct = {0};
    GPIO_BUTTON_InitStruct.Pin = GPIO_PIN_4 | GPIO_PIN_5;
    GPIO_BUTTON_InitStruct.Mode = GPIO_MODE_INPUT;

    HAL_GPIO_Init(GPIOC, &GPIO_BUTTON_InitStruct);
    HAL_Delay(20);

    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);

    setPwrKeyOn(true);

    return true;
}

void Gpio::setOrangeLed(bool level)
{
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, level ? GPIO_PIN_RESET : GPIO_PIN_SET);
}

void Gpio::setBlueLed(bool level)
{
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, level ? GPIO_PIN_RESET : GPIO_PIN_SET);
}

void Gpio::switchFastShutdown(bool level)
{
    // inverted levels
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, level ? GPIO_PIN_RESET : GPIO_PIN_SET);
}

void Gpio::setPwrKeyOn(bool level)
{
    // level is inverted, need to trigger RESET and PWR_KEY together
    if (!level)
    {
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6 | GPIO_PIN_7, GPIO_PIN_SET);
        return;
    }
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6 | GPIO_PIN_7, GPIO_PIN_RESET);
}
