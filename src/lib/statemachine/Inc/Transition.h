#ifndef STM32L0BREAKOUT_TRANSITION_H
#define STM32L0BREAKOUT_TRANSITION_H

#include "IState.h"

template<class StateID, class Event>
struct Transition{
    StateID currentState;
    Event event;
    IState<StateID, Event>* nextState;
};

#endif //STM32L0BREAKOUT_TRANSITION_H
